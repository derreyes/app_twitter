/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app_twitter;

import java.io.IOException;
import java.util.List;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.web.WebView;
import twitter4j.TwitterException;

/**
 *
 * @author Dario Erreyes
 */
public class VentanaRegistrarse {
    private VBox ventana;
    private WebView mapWebView;
    private TextField pin;
    private Label error;
    private Button btnRegistrarse;
    private String User,Password;
    private Twitter_metodos app=new Twitter_metodos();
    
    public VentanaRegistrarse(String User,String Password){
        this.User=User;
        this.Password=Password;
        ventana=new VBox(10);
        mapWebView=new WebView();
        pin=new TextField();
        error=new Label();        
        btnRegistrarse=new Button("Registrarse");
        btnRegistrarse.setDisable(true);
        ventana.setPadding(new Insets(20));
        OrganizarNodos();
        manejoEventos();
    }
    
    public void OrganizarNodos(){
        ventana.getChildren().addAll(mapWebView,pin,error,btnRegistrarse);
        ventana.setAlignment(Pos.CENTER);
        pin.setMaxWidth(300);
        mapWebView.getEngine().load(app.URL_Autorizacion());
        pin.setText("PIN...");
        
    }
    
    public void manejoEventos(){
        pin.setOnMouseClicked(e -> {if (pin.getText().equals("PIN...")) pin.clear(); });
        pin.setOnKeyReleased(e-> { 
            if (pin.getText().trim().length()>6) {
                btnRegistrarse.setDisable(false); }
            else if (pin.getText().trim().length()<7){
                btnRegistrarse.setDisable(true); }
        });
        btnRegistrarse.setOnAction(e-> crearUsuario());
    }

    public VBox getVentana() {
        return ventana;
    }
    
    public void crearUsuario(){
        error.setText("");
        try {
            List<String> Tokens=app.Autorizacion(pin.getText().trim());
            Usuario user=new Usuario(User,Password,Tokens.get(0),Tokens.get(1));
            new Archivo().añadirUsuario(user);
        } catch (TwitterException ex) {
            error.setText("El PIN ingresado es erroneo.");
            error.setTextFill(Color.RED);
            app=new Twitter_metodos();
            mapWebView.getEngine().load(app.URL_Autorizacion());
        } catch (IOException ex) {
        }
    }
    
}
