package app_twitter;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author Sueanny Moreno 
 */
public class IniciarSesion {
    private VBox pantalla=new VBox(10);
    private HBox areaTextoFoto=new HBox(5);
    private TextField postear=new TextField("What's happening?");
    private ImageView fotoPerfil,insertarFoto;
    private Button verNuevo=new Button();
    private String User,Password;
    
    
    public IniciarSesion(String User,String Password)throws FileNotFoundException{
        this.User=User;
        this.Password=Password;
        insertarFoto=new ImageView(new Image(new FileInputStream("src/Images/insert.png")));
        insertarFoto.setFitHeight(25);
        insertarFoto.setFitWidth(25);
        postear.setMinWidth(500);
        
        organizarControles();
        manejoEventos();
    }
    
    public void organizarControles(){
        pantalla.getChildren().addAll(areaTextoFoto,verNuevo);
        areaTextoFoto.getChildren().addAll(postear,insertarFoto);
        pantalla.setAlignment(Pos.CENTER);
        verNuevo.setMaxWidth(500);
    }
    
    public void manejoEventos(){
        postear.setOnMouseClicked(e -> {if (postear.getText().equals("What's happening?")) postear.clear(); });
    }

    public VBox getPantalla() {
        return pantalla;
    }
    
    
}
