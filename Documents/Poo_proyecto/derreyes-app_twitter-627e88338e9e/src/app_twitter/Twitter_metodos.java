/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app_twitter;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;

/**
 *
 * @author Dario Erreyes
 */
public class Twitter_metodos {
    private ConfigurationBuilder configBuilder = new ConfigurationBuilder().setDebugEnabled(true).setOAuthConsumerKey("NZZwJCDt8kBkTLxXHkt15iG0q").setOAuthConsumerSecret("xcpRVEsUIvXLQleoUB617bQiSaXrkpzFKRLXzI7QpVryTMrJe7");
    private twitter4j.Twitter OAuthTwitter = new TwitterFactory(configBuilder.build()).getInstance();
    private RequestToken requestToken=null;
    
    public Twitter_metodos(){
        try {
            requestToken = OAuthTwitter.getOAuthRequestToken();
            System.out.println("Request Tokens obtenidos con éxito.");
            System.out.println("Request Token: " + requestToken.getToken());
            System.out.println("Request Token secret: " + requestToken.getTokenSecret());
            } catch (TwitterException ex) {

            Logger.getLogger(TwitterJavaGT.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
    
    public String URL_Autorizacion(){
        String url=null;
        do {
            url = requestToken.getAuthorizationURL();
            System.out.println("URL:");
            System.out.println(url);
        }while (url == null);
        
        return url;
        
}
    
    public List<String> Autorizacion(String Pin) throws TwitterException{
        AccessToken accessToken =  OAuthTwitter.getOAuthAccessToken(requestToken, Pin);
        System.out.println("nnAccess Tokens obtenidos con éxito.");
        System.out.println("Access Token: " + accessToken.getToken());
        System.out.println("Access Token secret: " + accessToken.getTokenSecret());
        
        List<String> tokens=new ArrayList<>();
        tokens.add(accessToken.getToken()); tokens.add(accessToken.getTokenSecret()); 
        
        return tokens;
        
    }
    
    public ResponseList<Status> obtenerTweets(Usuario u) throws TwitterException{
        ConfigurationBuilder config= new ConfigurationBuilder().setDebugEnabled(true).setOAuthConsumerKey("NZZwJCDt8kBkTLxXHkt15iG0q").setOAuthConsumerSecret("xcpRVEsUIvXLQleoUB617bQiSaXrkpzFKRLXzI7QpVryTMrJe7").setOAuthAccessToken(u.getToken())
            .setOAuthAccessTokenSecret(u.getTokenSecret());
    
        Twitter twitter =  new TwitterFactory(config.build()).getInstance();
        //Recuperar listado de ultimos tweets escritos
        Paging pagina = new Paging();
        pagina.setCount(50);
        ResponseList<Status> listado = twitter.getHomeTimeline(pagina);
        //solo estouy imprimiendolos
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        for (Status status:listado) {  
        System.out.println("----------------------------------------------------------");
                System.out.println(String.format("User [%s]", status.getUser().getScreenName()));
                System.out.println("ur de la imagen" + status.getUser().getOriginalProfileImageURLHttps());
                System.out.println(status.getText());
                System.out.println(sdf.format(status.getCreatedAt()));
                System.out.println(String.format("RT[%d] FAV[%d]", status.getRetweetCount(), status.getFavoriteCount()));
                System.out.println("----------------------------------------------------------");}
        return listado;
    }
    
    public void EscribirTweet(Usuario u, String Tweet){
        ConfigurationBuilder config= new ConfigurationBuilder().setDebugEnabled(true).setOAuthConsumerKey("NZZwJCDt8kBkTLxXHkt15iG0q").setOAuthConsumerSecret("xcpRVEsUIvXLQleoUB617bQiSaXrkpzFKRLXzI7QpVryTMrJe7").setOAuthAccessToken(u.getToken())
            .setOAuthAccessTokenSecret(u.getTokenSecret());
        Twitter twitter =  new TwitterFactory(config.build()).getInstance();
        try{
        Status tweetEscrito = twitter.updateStatus(Tweet);
        }catch(Exception e){
            System.out.println(e);
            Alert cuadro=new Alert(Alert.AlertType.ERROR);
            cuadro.setHeaderText("ERROR");
            cuadro.setContentText("Ya has enviado este Tweet. ");
            cuadro.showAndWait();}
    }
    
    public void Retwittear(Usuario u, Status s) {
        ConfigurationBuilder config= new ConfigurationBuilder().setDebugEnabled(true).setOAuthConsumerKey("NZZwJCDt8kBkTLxXHkt15iG0q").setOAuthConsumerSecret("xcpRVEsUIvXLQleoUB617bQiSaXrkpzFKRLXzI7QpVryTMrJe7").setOAuthAccessToken(u.getToken())
            .setOAuthAccessTokenSecret(u.getTokenSecret());
        Twitter twitter =  new TwitterFactory(config.build()).getInstance();
        try {
            Status tweet = twitter.retweetStatus(s.getId());
        } catch (TwitterException ex) {
            System.out.println(ex);
            Alert cuadro=new Alert(Alert.AlertType.ERROR);
            cuadro.setHeaderText("ERROR");
            cuadro.setContentText("Ya has retwitiado este Tweet. ");
            cuadro.showAndWait();}
    }
    
    public void eventFavorite(Usuario u, Status s) throws TwitterException{
        ConfigurationBuilder config= new ConfigurationBuilder().setDebugEnabled(true).setOAuthConsumerKey("NZZwJCDt8kBkTLxXHkt15iG0q").setOAuthConsumerSecret("xcpRVEsUIvXLQleoUB617bQiSaXrkpzFKRLXzI7QpVryTMrJe7").setOAuthAccessToken(u.getToken())
            .setOAuthAccessTokenSecret(u.getTokenSecret());
        Twitter twitter =  new TwitterFactory(config.build()).getInstance();
        try{
            twitter.createFavorite(s.getId());}
        catch(Exception e){twitter.destroyFavorite(s.getId());}
    }
    
    
    /**

     * @throws java.io.IOException
     * @throws twitter4j.TwitterException
 */
    
    public static void buscarTwetts() throws IOException, TwitterException {
        ControladorTwitter controlador = new ControladorTwitter();
        String command = ControladorTwitter.readLine("Query in twitter (salir for exit)->>");
        while (!command.equals("salir")) {
            List<Status> result = controlador.query(command);
            controlador.mostrarEstado(result);
            command = ControladorTwitter.readLine("Query in twitter: ");
        }

        
    }}

    

