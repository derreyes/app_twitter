
package app_twitter;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.beans.property.ObjectProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.Stage;

/**
 *
 * @author Sueanny Moreno
 */
public class VentanaInicio  {
    
    private VBox root=new VBox(10);
    private final ImageView logo= new ImageView();
    private final TextField user= new TextField("Usuario...");
    private final TextField password= new TextField("Contraseña...");
    private final Button inicioSesion= new Button("Iniciar Sesion");
    private final Button registrarse= new Button("Registrarse");
    
    
    public VentanaInicio() throws FileNotFoundException{
        root.setPadding(new Insets(20));
        logo.setImage(new Image(new FileInputStream("src/Images/Twitter-TT.jpg")));
        logo.setFitHeight(300);
        logo.setFitWidth(300);
        organizarControles();
        Eventos();
    }
    
    public void organizarControles(){
        root.getChildren().addAll(logo, user,password,inicioSesion,registrarse);
        user.setMaxWidth(300);
        password.setMaxWidth(300);
        root.setAlignment(Pos.CENTER);
        
    }
    
    public void Eventos(){
        user.setOnMouseClicked(e -> {if (user.getText().equals("Usuario...")) user.clear(); });
        password.setOnMouseClicked(e -> {if (password.getText().equals("Contraseña...")) password.clear(); });
        registrarse.setOnAction(e->EventoRegistrarse());
        inicioSesion.setOnAction(i->{
            EventoInicioSesion();
        });
            
        }

    
    public void EventoRegistrarse(){
        String User=user.getText().trim();
        String Password=password.getText().trim();
        boolean existe=new Usuario().usuarioExistente(User);
        if (!(User.equals("") || User.equals("Usuario...") || Password.equals("") || Password.equals("Contraseña...") || existe )){
            root.getChildren().clear();
            root.getChildren().addAll(new VentanaRegistrarse(User,Password).getVentana().getChildren());
        }
        else if (existe){
            Alert cuadro=new Alert(Alert.AlertType.ERROR);
            cuadro.setHeaderText("ERROR");
            cuadro.setContentText("El usuario ya se encuentra registrado. ");
            cuadro.showAndWait();
        }
        else{
            Alert cuadro=new Alert(Alert.AlertType.ERROR);
            cuadro.setHeaderText("ERROR");
            cuadro.setContentText("Los parametros se encuentran vacios. ");
            cuadro.showAndWait();
        }
    }
    
         public void EventoInicioSesion() {
        String User=user.getText().trim();
        String Password=password.getText().trim();
        try{
        Usuario usuario=new Usuario().buscarUsuario(User, Password);
        if ((User.equals(usuario.getUser()))&&(Password.equals(usuario.getPassword()))){
            root.getChildren().clear();
            root.getChildren().addAll(new IniciarSesion(User,Password).getPantalla().getChildren());
            System.out.println("Error al buscar Usuario");
            
        }
        }catch(IOException  ex){
            System.out.println("");
        }
        boolean us=new Usuario().usuarioExistente(User);
        if (us != true){
             Alert cuadro=new Alert(Alert.AlertType.ERROR);
            cuadro.setHeaderText("ERROR");
            cuadro.setContentText("El Usuario no existe ");
            cuadro.showAndWait();
        }
    }
  public VBox getRoot() {
        return root;
    }
}

