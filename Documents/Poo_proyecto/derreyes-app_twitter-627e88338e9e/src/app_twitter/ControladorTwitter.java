
package app_twitter;

/**
 *
 * @author Sueanny Moreno
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.List;
 
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
 
public class ControladorTwitter {
    private Twitter twitter;
    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
 
    public ControladorTwitter() {
        twitter = TwitterFactory.getSingleton();
    }
 
    public List<Status> query(String query) throws TwitterException {
        QueryResult search = twitter.search(new Query(query));
        List<Status> tweets = search.getTweets();
        return tweets;
    }
 
    
    public List<Status> mostrarEstado(List<Status> status) {
        for (Status tweet : status) {
        System.out.println("----------------------------------------------------------");
        System.out.println(String.format("User [%s]", tweet.getUser().getScreenName()));
        System.out.println(tweet.getText());
        System.out.println(sdf.format(tweet.getCreatedAt()));
        System.out.println(String.format("RT[%d] FAV[%d]", tweet.getRetweetCount(), tweet.getFavoriteCount()));
        System.out.println("----------------------------------------------------------");
            
        }
        return status;
    }
    public static String readLine() throws IOException {
           InputStreamReader isr = new InputStreamReader(System.in);
           BufferedReader reader = new BufferedReader(isr);
           return reader.readLine();
       }

       public static String readLine(String message) throws IOException {
           System.out.print(message);
           return readLine();
       }
}
