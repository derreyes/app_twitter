/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app_twitter;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javafx.application.Application;
import javafx.beans.property.ObjectProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.Stage;

/**
 *
 * @author Sueanny Moreno
 */
public class VentanaPrincipal  {
    
    private VBox root=new VBox(10);
    private final ImageView logo= new ImageView();
    private final TextField user= new TextField("Usuario...");
    private final TextField password= new TextField("Contraseña...");
    private final Button inicioSesion= new Button("Iniciar Sesion");
    private final Button registrarse= new Button("Registrarse");
    
    public VentanaPrincipal() throws FileNotFoundException{
        root.setPadding(new Insets(20));
        logo.setImage(new Image(new FileInputStream("src/Images/Twitter-TT.jpg")));
        logo.setFitHeight(300);
        logo.setFitWidth(300);
        organizarControles();
        Eventos();
    }
    
    public void organizarControles(){
        root.getChildren().addAll(logo, user,password,inicioSesion,registrarse);
        user.setMaxWidth(300);
        password.setMaxWidth(300);
        root.setAlignment(Pos.CENTER);
        
    }
    
    public void Eventos(){
        user.setOnMouseClicked(e -> {if (user.getText().equals("Usuario...")) user.clear(); });
        password.setOnMouseClicked(e -> {if (password.getText().equals("Contraseña...")) password.clear(); });
        registrarse.setOnAction(e->MostrarVentana());
    }

    
    public void MostrarVentana(){
        String User=user.getText().trim();
        String Password=password.getText().trim();
        boolean existe=new Usuario().usuarioExistente(User);
        if (!(User.equals("") || User.equals("Usuario...") || Password.equals("") || Password.equals("Contraseña...") || existe )){
            root.getChildren().clear();
            root.getChildren().addAll(new VentanaRegistrarse(User,Password).getVentana().getChildren());
        }
        else if (existe){
            Alert cuadro=new Alert(Alert.AlertType.ERROR);
            cuadro.setHeaderText("ERROR");
            cuadro.setContentText("El usuario ya se encuentra registrado. ");
            cuadro.showAndWait();
        }
        else{
            Alert cuadro=new Alert(Alert.AlertType.ERROR);
            cuadro.setHeaderText("ERROR");
            cuadro.setContentText("Los parametros se encuentran vacios. ");
            cuadro.showAndWait();
        }
    }
    public VBox getRoot() {
        return root;
    }
}

