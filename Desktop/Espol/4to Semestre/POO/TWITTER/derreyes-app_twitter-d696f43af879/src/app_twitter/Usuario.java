/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app_twitter;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author Dario Erreyes
 */
public class Usuario implements Serializable{
    private String User, password,Token,TokenSecret;
    
    public Usuario() {
    }
    
    public Usuario(String User, String password, String Token, String TokenSecret) {
        this.User = User;
        this.password = password;
        this.Token = Token;
        this.TokenSecret = TokenSecret;
    }

    public String getUser() {
        return User;
    }

    public String getPassword() {
        return password;
    }

    public String getToken() {
        return Token;
    }

    public String getTokenSecret() {
        return TokenSecret;
    }
    
    public Usuario buscarUsuario(String user, String contrasena) {
        try{
        ArrayList<Usuario> users= new Archivo().crearListaUser();
        for (Usuario u:users){  
            if (u.getUser().equals(user) && u.getPassword().equals(contrasena))
                return u;
        }} catch(IOException e){}
        return null;
    } 
    
    public boolean usuarioExistente(String User){
        try{
        ArrayList<Usuario> users= new Archivo().crearListaUser();
        for (Usuario u:users){  
            if (u.getUser().equals(User))
                return true;
        }} catch(IOException e){}
        return false;
    }
}
