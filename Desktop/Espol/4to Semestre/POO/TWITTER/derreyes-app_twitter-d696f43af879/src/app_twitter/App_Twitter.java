/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app_twitter;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author Dario Erreyes
 */
public class App_Twitter extends Application {
    
   @Override
   public void start(Stage primaryStage) throws FileNotFoundException {       
        Scene scene = new Scene(new VentanaPrincipal().getRoot());
        primaryStage.setTitle("Twitter Desktop");
        primaryStage.setScene(scene);
        primaryStage.getIcons().add(new Image(new FileInputStream("src/Images/icono.png")));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch();
    } 
    
    
}
