/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app_twitter;

import javafx.application.Application;
import javafx.beans.property.ObjectProperty;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.Stage;

/**
 *
 * @author Sueanny Moreno
 */
public class VentanaPrincipal extends Application {
    
    private final VBox root=new VBox(10);
    private final ImageView logo= new ImageView();
    private final TextField user= new TextField("Usuario");
    private final TextField password= new TextField("contraseña");
    private final Button inicioSesion= new Button("Iniciar Sesion");
    private final Button registrarse= new Button("Registrarse");
    
    public VentanaPrincipal(){
        logo.setImage(new Image("http://www.audienciaelectronica.net/wp-content/uploads/2018/07/Twitter-TT.jpg",true));
        logo.setFitHeight(200);
        logo.setFitWidth(200);
        organizarControles();
        
    }
    
    public void organizarControles(){
        root.getChildren().addAll(logo, user,password,inicioSesion,registrarse);
        root.setAlignment(Pos.CENTER);
        
    }
    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
         Scene sc = new Scene(root);
        primaryStage.setScene(sc);
        primaryStage.show();
    }
}
