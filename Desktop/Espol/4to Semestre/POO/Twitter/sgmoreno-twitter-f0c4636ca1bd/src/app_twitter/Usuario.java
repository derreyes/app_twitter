/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app_twitter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author Dario Erreyes
 */
public class Usuario {
    private String User, password,Token,TokenSecret;

    public Usuario(String User, String password, String Token, String TokenSecret) {
        this.User = User;
        this.password = password;
        this.Token = Token;
        this.TokenSecret = TokenSecret;
    }

    public String getUser() {
        return User;
    }

    public String getPassword() {
        return password;
    }

    public String getToken() {
        return Token;
    }

    public String getTokenSecret() {
        return TokenSecret;
    }
    
    public Usuario buscarUsuario(String correo, String contrasena) throws IOException{
        ArrayList<Usuario> users= new Archivo().crearListaUser();
        for (Usuario u:users){  
            if (u.getUser().equals(correo) && u.getPassword().equals(contrasena))
                return u;
        }
        return null;
    }
    
    
}
