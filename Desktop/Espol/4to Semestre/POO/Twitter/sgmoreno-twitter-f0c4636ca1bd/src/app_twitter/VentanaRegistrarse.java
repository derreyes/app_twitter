/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app_twitter;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;

/**
 *
 * @author Dario Erreyes
 */
public class VentanaRegistrarse {
    private VBox ventana;
    private WebView mapWebView;
    private TextField pin;
    private Button btnRegistrase;
    
    public VentanaRegistrarse(){
        ventana=new VBox(10);
        mapWebView=new WebView();
        pin=new TextField();
        btnRegistrase=new Button("Registrarse");
        btnRegistrase.setDisable(true);
        ventana.setPadding(new Insets(20));
        OrganizarNodos();
        manejoEventos();
    }
    
    public void OrganizarNodos(){
        ventana.getChildren().addAll(mapWebView,pin,btnRegistrase);
        ventana.setAlignment(Pos.CENTER);
        mapWebView.getEngine().load(new Twitter().URL_Autorizacion());
        pin.setText("PIN...");
        
    }
    
    public void manejoEventos(){
        pin.setOnMouseClicked(e -> {if (pin.getText().equals("PIN...")) pin.clear(); });
        pin.setOnKeyReleased(e-> { 
            if (pin.getText().trim().length()>6) {
                btnRegistrase.setDisable(false); }
            else if (pin.getText().trim().length()<7){
                btnRegistrase.setDisable(true); }
        });    
    }

    public VBox getVentana() {
        return ventana;
    }
    
    
}
