/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app_twitter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;



/**
 *
 * @author Dario Erreyes
 */
public class Archivo {
    private ArrayList<Usuario> listUsers=new ArrayList();
    
    public  void añadirUsuario(Usuario user) throws IOException {
        try(ObjectInputStream leer = new ObjectInputStream(new FileInputStream(new File("Usuario.txt")))){
            Object o=leer.readObject();
            listUsers=(ArrayList<Usuario>) o;            
        }catch(ClassNotFoundException | IOException  ex){}
        
        listUsers.add(user);
        try(ObjectOutputStream escribir = new ObjectOutputStream(new FileOutputStream( new File("Usuario.txt")))){
            escribir.writeObject(listUsers);
        }catch(IOException e){}
    }
    
    public  ArrayList<Usuario> crearListaUser() throws IOException{
        ArrayList<Usuario> usuarios=new ArrayList<>();
        try(ObjectInputStream leer = new ObjectInputStream(new FileInputStream(new File("Usuario.txt")))){
            Object o;
            if ((o=leer.readObject())!=null){
                usuarios=(ArrayList<Usuario>) o;           
            }
        }catch(ClassNotFoundException | IOException  ex){}
        
         return usuarios;
    }
}
